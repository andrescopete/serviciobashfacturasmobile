﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioServicio
{
    public partial class Form1 : Form
    {
        private ServicioPrueba.Service1 servicio;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
            servicio = new ServicioPrueba.Service1();
            servicio.StartService();
                label1.Text = "Servicio iniciado correctamente";
                label2.Text = "";
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepcion start formulario:" + ex.ToString());
                label1.Text = "Error al Iniciar servicio";
                label2.Text = "";
                richTextBox1.Text = e.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
            servicio.StopService();
                label2.Text = "Servicio finalizo correctamente";
                label1.Text = "";
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excepcion stop formulario:" + ex.ToString());
                label2.Text = "Error al finalizar servicio";
                label1.Text = "";
                richTextBox1.Text = e.ToString();
            }
        }
    }
}
