﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using iAnywhere.Data.SQLAnywhere;
using System.Threading;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace ServicioPrueba
{
    public partial class Service1 : ServiceBase
    {
       
        private static SAConnection conexionDB = null;
        private string Transact;
        private System.Timers.Timer timer;
        public Service1()
        {
            try { 
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";
            }catch(Exception e)
            {
                Console.WriteLine("Excepcion busqueda ruta:" + e.ToString());
            }
        }
        protected override void OnStart(string[] args)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 20000; // 60 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            //timer.Enabled = true;
            timer.Start();
            eventLog1.WriteEntry("Inicie el servicio para ejecutar el query de solucion");

        }

        //Probar servicio desde formulario de prueba para validar funcionamiento
        public void StartService()
        {
            try { 
            timer = new System.Timers.Timer();
            timer.Interval = 50000; // 60 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Enabled = true;
            timer.Start();
            eventLog1.WriteEntry("Inicie el servicio para ejecutar el query de solucion");
            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion al Iniciar servicio:"+e.ToString());
                Console.WriteLine("Excepcion:" + e.ToString());
            }
        }


        public void StopService()
        {
            try
            {
            eventLog1.WriteEntry("Parando Servicio de envio");
            timer.Stop();

            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion al finalizar servicio:" + e.ToString());
            }
        }


        protected override void OnStop()
        {
            eventLog1.WriteEntry("Parando Servicio de envio");
            timer.Stop();

        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            try {

                timer.Enabled = false;
                sendJsonMobile();

                /*DateTime fecha = DateTime.Now;
                eventLog1.WriteEntry("FECHA ACTUAL:" + fecha.ToShortDateString());
                var ventas = getBashVentas();

                foreach (DataRow item in ventas.Tables[0].Rows)
                {
                    eventLog1.WriteEntry("TABLA:" + item["transact"]+"-" + item["timestart"]+"-"+ item["timeend"] + "-" + item["nettotal"] + "-" + item["whostart"]+"-"+item["finaltotal"]);
                    Transact = item["transact"].ToString();
                }
                    Properties.Settings.Default.Reset();
                    Properties.Settings.Default.Transact = Transact;
                    Properties.Settings.Default.Save();

                eventLog1.WriteEntry("TRANSACT ONTIMER:" + Transact);

                string jsonFormat = JsonConvert.SerializeObject(ventas, Formatting.Indented);
                eventLog1.WriteEntry("JSON A ENVIAR:" + jsonFormat);
                var client = new RestClient("https://xh3gjavtz9.execute-api.us-west-2.amazonaws.com/dev/getdataservice");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonFormat, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                eventLog1.WriteEntry("RESPUESTA:" + response.Content);*/


            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion:" + e.ToString());
            }
        }



        public void sendJsonMobile()
        {


            try
            {
                //timer.Enabled = false;
                eventLog1.WriteEntry("Valor de la propiedad:" + Properties.Settings.Default.Transact);
                Transact = Properties.Settings.Default.Transact;
                DateTime fecha = DateTime.Now;
                //string fechaActual = fecha.ToShortDateString();
                string fechaActual = fecha.ToString("dd-MM-yyyy");
                eventLog1.WriteEntry("FECHA CONVERSION:" + '+' + fechaActual + '+' + "\'" + fechaActual + "\'" + "BANDERA TRANSACT:" + Transact);
                var fechaFormated = "\'" + fechaActual + "\'";

                SAConnection conexionDB = new SAConnection("Data Source = PixelSQLbase; UID = reportuser; PWD = pixel1047");
                conexionDB.Open();
                eventLog1.WriteEntry("VALOR DE LA CONEXION:" + conexionDB.State.ToString() + conexionDB.Database.ToString() + "BD" + conexionDB.DataSource.ToString());
                string query = "SELECT ph.transact, ph.timestart, ph.timeend, ph.nettotal,ph.whostart,ph.finaltotal,sys.contact  FROM DBA.POSHEADER PH, DBA.SYSINFO SYS WHERE convert(varchar(10),timestart,105 )=" + fechaFormated + " AND  ph.transact >" + Transact;
                eventLog1.WriteEntry("QUERY:" + query);
                SACommand comando = new SACommand(query, conexionDB);
                SADataAdapter adapter = new SADataAdapter(comando);

                var ventas = new DataSet();
                adapter.Fill(ventas);
                conexionDB.Close();






                //DateTime fecha = DateTime.Now;
                eventLog1.WriteEntry("FECHA ACTUAL:" + fecha.ToShortDateString());
                eventLog1.WriteEntry("TRANSACT ALMACENADA ANTES DEL FOR:" + Properties.Settings.Default.Transact);
                // ventas = getBashVentas();

                foreach (DataRow item in ventas.Tables[0].Rows)
                {
                    eventLog1.WriteEntry("TABLA:" + item["transact"] + "-" + item["timestart"] + "-" + item["timeend"] + "-" + item["nettotal"] + "-" + item["whostart"] + "-" + item["finaltotal"]);
                    Transact = item["transact"].ToString();
                    eventLog1.WriteEntry("TRANSACCIONES FOR:" +Transact);
                    Properties.Settings.Default.Transact = Transact;
                }

                Properties.Settings.Default.Save();
                eventLog1.WriteEntry("TRANSACT ALMACENADA:" + Properties.Settings.Default.Transact);
                eventLog1.WriteEntry("TRANSACT ALMACENADA:" + Transact);
                Properties.Settings.Default.Transact = Transact;
                Properties.Settings.Default.Save();

                eventLog1.WriteEntry("TRANSACT ONTIMER:" + Transact);

                string jsonFormat = JsonConvert.SerializeObject(ventas, Formatting.Indented);
                eventLog1.WriteEntry("JSON A ENVIAR:" + jsonFormat);
                var client = new RestClient("https://xh3gjavtz9.execute-api.us-west-2.amazonaws.com/dev/getdataservice");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonFormat, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                eventLog1.WriteEntry("RESPUESTA:" + response.Content);

                timer.Enabled = true;
            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion:" + e.ToString());
            }







        }

        public bool getBashVentas()
        {

            try
            {
                eventLog1.WriteEntry("Valor de la propiedad:" + Properties.Settings.Default.Transact);
                Transact = Properties.Settings.Default.Transact;
                DateTime fecha = DateTime.Now;
                //string fechaActual = fecha.ToShortDateString();
                string fechaActual = fecha.ToString("dd-MM-yyyy");
                eventLog1.WriteEntry("FECHA CONVERSION:" + '+'+fechaActual+'+' + "\'" + fechaActual + "\'" + "BANDERA TRANSACT:"+Transact);
                var fechaFormated = "\'" + fechaActual + "\'";

                SAConnection conexionDB = new SAConnection("Data Source = PixelSQLbase; UID = reportuser; PWD = pixel1047");
                conexionDB.Open();
                eventLog1.WriteEntry("VALOR DE LA CONEXION:" + conexionDB.State.ToString() + conexionDB.Database.ToString() +"BD"+ conexionDB.DataSource.ToString());
                string query = "SELECT ph.transact, ph.timestart, ph.timeend, ph.nettotal,ph.whostart,ph.finaltotal,sys.contact  FROM DBA.POSHEADER PH, DBA.SYSINFO SYS WHERE convert(varchar(10),timestart,105 )="+fechaFormated+" AND  ph.transact >"+Transact;
                eventLog1.WriteEntry("QUERY:" + query);
                SACommand comando = new SACommand(query, conexionDB);
                SADataAdapter adapter = new SADataAdapter(comando);

                var ds = new DataSet();
                adapter.Fill(ds);

                //sendJsonMobile(ds);



                /*foreach(DataRow item in ds.Tables[0].Rows)
                {
                eventLog1.WriteEntry("TABLA:" + item["transact"]);
                    
                }*/                
              conexionDB.Close();
                return true;
              //return ds;
            }
            catch (Exception e)
            {
               eventLog1.WriteEntry("Excepcion:" + e.ToString());
                return false;
               //return null;
            }

        }
        







        public string getVentas()
        {
           try
            {
            var sumtotal = "";
            SAConnection conexionDB = new SAConnection("Data Source = PixelSQLbase; UID = reportuser; PWD = pixel1047");
            conexionDB.Open();
            eventLog1.WriteEntry("VALOR DE LA CONEXION:" + conexionDB.State.ToString() + conexionDB.Database.ToString() + conexionDB.DataSource.ToString());
            string query = "SELECT SUM(PH.NETTOTAL) AS TOTAL FROM DBA.POSHEADER PH";
            SACommand comando = new SACommand(query, conexionDB);
            SADataReader reader = comando.ExecuteReader();
            eventLog1.WriteEntry("VALOR DEL READER:");

            if (reader.Read())
            {
                sumtotal = reader.GetValue(0).ToString();
                eventLog1.WriteEntry("NETTOTAL:" + reader.GetValue(0).ToString());
            }
            else
            {
                eventLog1.WriteEntry("ELSE:" + reader.Read().ToString());
            }

            reader.Close();
            conexionDB.Close();

            return sumtotal;
            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion:" + e.ToString());
                return "";
            }
        }

        public string getAnulaciones()
        {

            try
            {
                var anulaciones = "";
                SAConnection conexionDB = new SAConnection("Data Source = PixelSQLbase; UID = reportuser; PWD = pixel1047");
                conexionDB.Open();
                eventLog1.WriteEntry("VALOR DE LA CONEXION:" + conexionDB.State.ToString() + conexionDB.Database.ToString() + conexionDB.DataSource.ToString());
                string query = "select  count(pd.transact) as anulaciones from dba. posheader ph inner join dba.posdetail pd on  ph.transact = pd.transact where pd.prodtype = 101";
                SACommand comando = new SACommand(query, conexionDB);
                SADataReader reader = comando.ExecuteReader();
                eventLog1.WriteEntry("VALOR DEL READER:");

                if (reader.Read())
                {
                    anulaciones = reader.GetValue(0).ToString();
                    eventLog1.WriteEntry("ANULACIONES:" + reader.GetValue(0).ToString());
                }
                else
                {
                    eventLog1.WriteEntry("ELSE ANULAICONES:" + reader.Read().ToString());
                }

                reader.Close();
                conexionDB.Close();

                return anulaciones;
            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion:" + e.ToString());
                return "";
            }

        }

        public string getDescuentos()
        {

            try
            {
                var descuentos = "";
                SAConnection conexionDB = new SAConnection("Data Source = PixelSQLbase; UID = reportuser; PWD = pixel1047");
                conexionDB.Open();
                eventLog1.WriteEntry("VALOR DE LA CONEXION:" + conexionDB.State.ToString() + conexionDB.Database.ToString() + conexionDB.DataSource.ToString());
                string query = "select  count(pd.transact) as anulaciones from dba. posheader ph inner join dba.posdetail pd on  ph.transact = pd.transact where pd.prodtype = 100";
                SACommand comando = new SACommand(query, conexionDB);
                SADataReader reader = comando.ExecuteReader();
                eventLog1.WriteEntry("VALOR DEL READER:");

                if (reader.Read())
                {
                    descuentos = reader.GetValue(0).ToString();
                    eventLog1.WriteEntry("DESCUENTOS:" + reader.GetValue(0).ToString());
                }
                else
                {
                    eventLog1.WriteEntry("ELSE DESCUENTOS:" + reader.Read().ToString());
                }

                reader.Close();
                conexionDB.Close();

                return descuentos;
            }
            catch (Exception e)
            {
                eventLog1.WriteEntry("Excepcion:" + e.ToString());
                return "";
            }
        }

        public string pagosPorMetodo()
        {
            return "";
        }
    }
}
    

